note
	description: "A DevGAmer is a Person which has access to commands of both the Gamer and the Developer"
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

class
	DEVGAMER

inherit
	DEV
		undefine
			toString
		end
	GAMER
		undefine
			toString
		end

create
	make

feature {NONE}
	toString: STRING
	do
		Result := "DEVGAMER: " + FirstName + " " + LastName
	end
end
