note
	description: "A Gamer is a Person which has access to specific commands"
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

class
	GAMER

inherit
	PERSON

create
	make

feature {NONE}
	toString: STRING
	do
		Result := "GAMER: " + FirstName + " " + LastName
	end
end
