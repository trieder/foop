note
	description: "The abstract base class for any person in our application"
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PERSON

feature {ANY}

	FirstName : STRING
	--the the first name of the person

	LastName : STRING
	--the the last name of the person

	toString: STRING
	--returns a string representation of the name of the person of the command
	deferred
	end

	-- constructor to be used in the derived classes
	make (fName: STRING; lName: STRING)
	do
		firstName := fName
		lastName := lName
	end

invariant
	-- make sure first and last name are not empty
	first_name_not_void: FirstName /= VOID
	first_name_not_empty: FirstName.count > 0
	last_name_not_void: LastName /= VOID
	last_name_not_empty: LastName.count > 0
end
