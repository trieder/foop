note
	description: "Stores persons (and it's decendants)"
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

class
	PERSONSTORE

create
	make

feature

	-- add a person to the store - since person has invariants, we don't need a require/ensure here
	addPerson (person: PERSON)
	require
		not persons.has (person)
	do
	 	persons.extend(person)
	end

	-- returns a copy of all the persons stored
	getPersons: LINKED_LIST [PERSON]
	local
		personsCopy: LINKED_LIST [PERSON]
	do
		create personsCopy.make
		personsCopy.copy (persons)
		Result := personsCopy
	end

	-- returns the index of the person with the given first and last name; -1 if there is no such person
	getIndexOf (firstName: String; lastName: String): INTEGER
	require
		firstName /= void
		firstName.count > 0
		lastName /= void
		lastName.count > 0
	local
		index: INTEGER
		i: INTEGER
	do
		index := -1
		i := 1
		across persons as person
	 	loop
			if person.item.firstname.is_equal (firstName) and person.item.lastname.is_equal (lastName) then
	  			index := i
	  		end
	  		i := i + 1
		end
		Result := index
	end

	-- returns the item at index i
	getItemAtIndex (index: INTEGER): Person
	require
		index > 0
		index <= persons.count
	do
		Result := persons.i_th (index)
	end

	persons : LINKED_LIST [PERSON]

feature {NONE}

	make
	do
		create persons.make
	end

invariant
	persons /= VOID
end
