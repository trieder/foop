note
	description: "A Developer is a Person which has access to specific commands"
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

class
	DEV

inherit
	PERSON

create
	make

feature {NONE}
	toString: STRING
	do
		Result := "DEV: " + FirstName + " " + LastName
	end
end
