note
	description: "A factory that makes creating new persons more convinient"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSONFACTORY

create
	make

feature {NONE}
	make
	do

	end

feature

	-- invariant in the person makes sures that firstName and lastName are not empty
	buildDeveloper (firstName: STRING; lastName: STRING): DEV
	local
		dev: DEV
	do
		create dev.make (firstName, lastName)
		Result := dev
	end

	buildGamer (firstName: STRING; lastName: STRING): GAMER
	local
		gamer: GAMER
	do
		create gamer.make (firstName, lastName)
		Result := gamer
	end

	buildDeveloperGamer (firstName: STRING; lastName: STRING): DEVGAMER
	local
		devgamer: DEVGAMER
	do
		create devgamer.make (firstName, lastName)
		Result := devgamer
	end

end
