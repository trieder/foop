note
	description: "Commandreader, reads commands from the {COMMANDREADER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMMANDREADER
	create
		make

	feature

	printCommands
	-- Prints all Commands currently listet with the commandreader
	do
		io.put_string("COMMANDS:")
		io.new_line
		io.put_string ("-----------------")
		io.new_line
	  	across commands as c
	  	loop
	  		io.put_string (c.item.name)
	  		io.new_line
	  	end
	  	io.put_string ("-----------------")
	  	io.new_line
	end

	addCommand (command: COMMAND)
	--Adds a command to the commandreader
	require
		command /= Void
	do
		commands.extend (command)
	end


	findCommand (cmdString : STRING) : COMMAND
	--finds a command for the given String
	local
		nocommand : NOCOMMAND
	do
		create nocommand.make (cmdString)
		Result := nocommand
		across commands as c
	  	loop
	  		if c.item.name.is_equal (cmdString) then
	  			Result := c.item
	  		end
	  	end
	end


	feature {NONE}
	commands : LINKED_SET [COMMAND]
	--Commands, linked set so we have no command issued twice
	stpCmd : STOPCOMMAND

	make
	do
		create commands.make
		create stpCmd.make
		commands.extend (stpCmd)
	end

end
