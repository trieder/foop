note
	description: "Stores a history of games and provides methods to retrieve them"
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

class
	GAMELOG
create
	make

feature

	-- Returns the last game added to the log
	lastGame : GAME
	require
		games.count > 0
	do
		Result := games.last
	end

	-- adds a new game to the log
	addGame (game : GAME)
	require
		-- game has its own invariants which is why we don't need a lot of requires
		game /= void
		not games.has (game)
	do
	 	games.extend(game)
	end

	-- returns the index of the game with the given name; -1 if there is no such game
	getIndexOf (gameName: String): INTEGER
	require
		gameName /= void
		gameName.count > 0
	local
		index: INTEGER
		i: INTEGER
	do
		index := -1
		i := 1
		across games as game
	 	loop
			if (game.item.name.is_equal (gameName)) then
	  			index := i
	  		end
	  		i := i + 1
		end
		Result := index
	end

	-- returns the item at index i
	getItemAtIndex (index: INTEGER): Game
	require
		index > 0
		index <= games.count
	do
		Result := games.i_th (index)
	end

	-- we have a precondition that involves this collection
	games : LINKED_LIST [GAME]

feature {NONE}

	make
	do
		create games.make
	end

invariant
	games /= VOID
end
