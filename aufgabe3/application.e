note
	description : "FOOP3-Application"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	make
		-- Run application.
		local
			commandreader : COMMANDREADER
			command : COMMAND
			stop : BOOLEAN

			hicommand : HICOMMAND
			creategamecommand : CREATEGAMECOMMAND
			showlastgame : SHOWLASTGAMECOMMAND
			showcatcall : SHOWCATCALLCOMMAND
			addparticipation : ADDPARTICIPATIONCOMMAND
			showpersons : SHOWPERSONS

			gamelog : GAMELOG
			devgame : DEVGAME
			personstore : PERSONSTORE
			personfactory : PERSONFACTORY

		do


		 	io.put_string("Welcome to Racing-Game Administration.")
		 	io.new_line
			create commandreader.make
			create gamelog.make
			create personstore.make
			create personfactory.make

			-- add some people to the store
			personstore.addperson (personfactory.builddeveloper ("Thomas", "Rieder"))
			personstore.addperson (personfactory.builddeveloper ("Patrick", "Saeuerl"))

			personstore.addperson (personfactory.buildgamer ("Manuel", "Geier"))
			personstore.addperson (personfactory.buildgamer ("Markus", "Zisser"))

			personstore.addperson (personfactory.builddevelopergamer ("Florian", "Mayer"))
			personstore.addperson (personfactory.builddevelopergamer ("Lisa", "Schuh"))

			--initaliz the commands
			create hicommand.make
			create creategamecommand.make (gamelog)
			create showlastgame.make (gamelog)
			create showcatcall.make
			create addparticipation.make (gamelog, personstore)
			create showpersons.make(personstore)

			commandreader.addcommand (hicommand)
			commandreader.addcommand (creategamecommand)
			commandreader.addcommand (showlastgame)
			commandreader.addcommand (showcatcall)
			commandreader.addcommand (addparticipation)
			commandreader.addcommand (showpersons)

			commandreader.printCommands
			--initalize other things

			--Read everything
			from stop := false
			until stop = true
			loop
				io.new_line
				io.put_string ("Command: ")
				io.read_line
				command := commandreader.findcommand (io.last_string)
				command.execute
				stop := command.stopexecution
			end
			--Specification of Commands for the CommandReader

		end

end
