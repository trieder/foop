note
	description: "a participation that can only be used by a developer"
	author: "Lisa Fichtinger"
	date: "$Date$"
	revision: "$Revision$"

class
	DEVPARTICIPATION

inherit
	PARTICIPATION

create
	make

end
