note
	description: "base class for participation that can be used by any person"
	author: "Lisa Fichtinger"
	date: "$Date$"
	revision: "$Revision$"

class
	PARTICIPATION

create
	make

feature

	TotalTime : INTEGER
	--returns the TOTAL time of all laps
	local
		sumTime : INTEGER
	do
		sumTime := 0
		across paLapTimes as t
		 loop sumTime := sumTime + t.item  end
		Result := sumTime
	end

	LapCount : INTEGER
	--returns the amounts of lap driven
	do
		Result := paLapTimes.count
	end

	addLap (time : INTEGER)
	--add time of a lap to the participation
	do
		paLapTimes.extend (time)
	end

	toString : STRING
	local
		str : STRING
		do

			str := "%N -- Participation "
			str.append (paPlayer.tostring)
			str.append (" --")
			across paLapTimes as t
			loop
				str.append ("%N Lap ")
				str.append_integer (t.cursor_index)
				str.append (" : ")
				str.append_integer (t.item)
			end
			str.append ("%N -----")
			str.append ("%N Lap Count: ")
			str.append_integer (lapcount)
			str.append ("%N Total Time: ")
			str.append_integer (totaltime)
			Result := str
		end

feature {NONE}
	-- private variables and routines
	paPlayer : PERSON
	paLapTimes : LINKED_LIST [INTEGER]

	make (player : PERSON)
	do
		paPlayer := player
		create paLapTimes.make

	end

	invariant
		player_not_void : paPlayer /= VOID
		times_list_not_void : paLapTimes /= VOID


end
