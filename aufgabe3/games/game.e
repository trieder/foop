note
	description: "a class representation a f1 game - identified by name, duration and number of laps"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature

	Name : STRING
	--returns the Name of the game
	do
		Result := prName
	end

	Duration : INTEGER
	--returns the Duration of the game
	do
		Result := prDuration
	end

	Laps : INTEGER
	--returns the Laps of the game
	do
		Result := prLaps
	end

	AddParticipation (part : Participation)
	--adds a participation of a participant
	require
		part.lapcount >= 0
		part.lapcount <= Laps
		part.totaltime <= Duration
		part.totaltime >= 0
	do
		prParticipations.extend (part)
	end

	toString : STRING
	--retrusn this Game for printing
	local
			-- here you can define local variables for this function
			str: STRING
	do
			str := ""
			str.append (type)
			str.append ("%N Name: ")
			str.append (name)
			str.append ("%N Laps: ")
			str.append_integer (laps)
			str.append ("%N Duration: ")
			str.append_integer (duration)
			across prParticipations as part loop str.append (part.item.tostring)  end
			Result := str
	end

	type : String
	--returns the GameType
	do
		Result := "Game"
	end


feature {NONE}
	--private variables, game might not be changed after created
	prName : STRING
	prDuration : INTEGER
	prLaps : INTEGER
	prParticipations : LINKED_LIST [PARTICIPATION]

	make (gamename : STRING; gameduration : INTEGER; gamelaps : INTEGER)
	do
		prName := gamename
		prDuration := gameduration
		prLaps := gamelaps
		create prParticipations.make
	end

	invariant
		name_not_void: Name /= VOID
		name_not_empty: Name.count > 0
		duration_set : prDuration /= VOID
		duration_gt_zero : prDuration > 0
		duration_less_than_a_day : prDuration < 86400
		prLaps_gt_zero : prLaps > 0
		duration_bigger_than_laps : prDuration > prLaps --10 laps in 5 duration ... impossible
end
