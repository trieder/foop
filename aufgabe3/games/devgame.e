note
	description: "a subclass of {GAME} that can only be driven by developers"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DEVGAME

inherit
	GAME redefine AddParticipation, type end

create
	make
	--needs to be defined so we know that game.make is used
feature

	type : String
	--returns the GameType
	do
		Result := "DevGame"
	end

	AddParticipation (part : DEVPARTICIPATION)
	--adds a participation of a participant
	do
		prParticipations.extend (part)
	end
end
