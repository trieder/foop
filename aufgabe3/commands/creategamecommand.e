note
	description: "creates a new game based on the user input and adds it to the log"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CREATEGAMECOMMAND

inherit
	COMMAND

create
	make

feature

	Name : STRING
	--Returns HI, the name of this class
	do
		Result := "CREATEGAME"
	end

	execute
	--creates a new game
	local
		game : GAME
		devgame : DEVGAME

		gameType : STRING
		gname : STRING
		duration : INTEGER
		laps : INTEGER
	do
		io.new_line
		print ("Create New Game:")
		io.new_line
		print("Type - 0 for normal Game, 1 for TestGame: ")
		io.read_line
		create gameType.make_from_string (io.last_string)
		io.new_line
		if(not (gameType.is_equal ("0") or gameType.is_equal ("1")))
		then
				io.new_line
				print("Wrong input, exiting Create Game")
				io.new_line
		else

			io.new_line
			print("Name of game: ")
			io.read_line
			create gname.make_from_string (io.last_string)

			io.new_line
			print("Duration: ")
			io.read_integer
			duration := io.last_integer

			io.new_line
			print("Laps: ")
			io.read_integer
			laps := io.last_integer


			--could have used inspect instead
			io.new_line
			print("----------------")
			io.new_line
			print ("Adding Game")
			io.new_line

			if(gameType.is_equal("0")) then
				create game.make (gname, duration, laps)
				print(game.tostring)
				gamelog.addgame (game)
			else
				if(gameType.is_equal("1")) then
					create devgame.make (gname, duration, laps)
					print(devgame.tostring)
					gamelog.addgame (devgame)
				else
					print("Should not occur")
				end
			end

			io.new_line
			print("----------------")
		end

	end

feature {NONE}
	gamelog : GAMELOG
	--the gamelog where the new created game is stored

	make (log :GAMELOG)
	do
		gamelog := log
	end

invariant
	gamelog_not_null : gamelog /= VOID

end
