note
	description: "Base class for {COMMAND}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	--Base class for Commnands used in our application
	COMMAND
inherit

	feature

		Name : STRING
		--the name of the command
		deferred
		end

		execute
		--execution of the command
		deferred
		end

		stopExecution : BOOLEAN
		--Returns if after this command, the execution of the outer program should stop
		do
			Result := false
		end

	invariant
		name_not_void: Name /= VOID
		name_not_empty: Name.count > 0
end
