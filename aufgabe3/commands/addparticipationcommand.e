note
	description: "handles the creation of a new participation"
	author: "Lisa Fichtinger"
	date: "$Date$"
	revision: "$Revision$"

class
	ADDPARTICIPATIONCOMMAND

inherit
	COMMAND

create
	make

feature

	Name : String
	-- Returns the name of this class
	do
		Result := "ADDPARTICIPATION"
	end

	execute
	-- creates a new participation and adds it to the game
	local
		game : GAME
		gameIdx: INTEGER
		part : PARTICIPATION
		devpart : DEVPARTICIPATION
		firstName: STRING
		lastName: STRING
		personIdx: INTEGER
		person: PERSON
		str: STRING

	do

		io.new_line

		--list all games to select one
		if (coGamelog.games.count < 1) then
			print("No games found - can not add participation")

		else

			print ("---- Games ----")
			across coGamelog.games as g
				loop
					str := "%N"
					str.append (g.item.name)
					print(str)
				end
			print("%N----------")
			print("%NEnter the name of the game: ")
			io.read_line

			gameIdx := coGamelog.getindexof (io.last_string)
			io.new_line

			if (gameIdx.is_equal (-1)) then
				-- no game was found with the name
				print("Wrong input - can not add participation.")
			else
				game := coGamelog.getitematindex (gameIdx)

				-- list all persons from the personstore to select
				print("---- Persons ----")
				if(coPersonstore.getpersons.count < 1) then
					print("%NNo persons available.")
				else
					across coPersonstore.getpersons as p
					loop
						str := "%N"
						str.append (p.item.firstName)
						str.append (" ")
						str.append(p.item.lastName)
						print (str)
					end

					print("%N----------")

					-- select person
					print("%NEnter first name of player: ")
					create firstName.make_empty
					io.read_line
					firstName.copy (io.last_string)

					print("Enter last name of player: ")
					create lastName.make_empty
					io.read_line
					lastName.copy (io.last_string)

					personIdx := coPersonstore.getindexof (firstName, lastName)

					if personIdx.is_equal (-1) then
						print ("%NNo person found for the given input.")
						io.new_line

					else
						person := coPersonstore.getitematindex (personIdx)
						-- create participation and add laps
						create part.make (person)

						if game.type.is_equal ("DevGame") then
							create devpart.make (person)
							part := devpart
						end

						part := doLaps (part, game)
						game.addparticipation (part)
						io.new_line
						print("-----------")
						print("%N Added Participation")
						print("%N Game: ")
						print(game.name)
						print(part.tostring)
						print("%N-----------")


					end
				end
			end
		end
	end


feature {NONE}
	-- private variables and routines
	coGamelog : GAMELOG
	coPersonstore : PERSONSTORE

	make (gamelog : GAMELOG; personstore : PERSONSTORE)
	do
		coGamelog := gamelog
		coPersonstore := personstore
	end

	doLaps (part: PARTICIPATION; game : GAME) : PARTICIPATION
	require part_not_void : part /= VOID

	local
		i : INTEGER
		str : STRING
		time : INTEGER

	do
		from i := 1
		until i > game.laps
		loop
			str := "Enter run time for lap ";
			str.append_integer (i)
			str.append (" : ")
			io.new_line
			print (str)
			io.read_integer
			time := io.last_integer
			part.addlap (time)
			i := i + 1
		end

		RESULT := part

	end

invariant
	--gamelog and personstore must not be empty
	gamelog_set: coGamelog /= VOID
	personstore_set: coPersonstore /= VOID

end
