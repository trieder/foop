note
	description: "command to exit the application"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	STOPCOMMAND

inherit
	COMMAND
	redefine
		stopExecution
		end
create
	make

feature

	Name : STRING
	do
		Result := "STOP"
	end

	execute
	do
		io.new_line
		print("Stopping App, Goodbye!")
	end

	stopExecution : BOOLEAN
	--Returns true to stop the execution
		do
			Result := TRUE
		end



feature {NONE} -- Initialization
	make
	-- Initialization for `Current'.
	do end

end
