note
	description: "is returned if no command can be found"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NOCOMMAND
	--Command that retusn NOCommand has been found

inherit
	COMMAND

create
	make

feature

	Name : STRING
	do
		Result := "NOCMD"
	end

	execute
	do
		io.new_line
		print ("No command found for input: ")
		print (cmdText)
		io.new_line
	end



feature {NONE} -- Initialization
	cmdText : STRING

	make (command : STRING)
	-- Initialization for `Current', add the text for output
	do
		cmdText := command
	end

invariant
	cmdText_not_void: cmdText /= Void
end
