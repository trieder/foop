note
	description: "TestCommand, printing hi."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HICOMMAND

inherit
	COMMAND

create
	make

feature
	Name : STRING
	--Returns HI, the name of this class
	do
		Result := "HI"
	end

	execute
	--Prints hi
	do
		io.new_line
		print ("HI from HICOMMAND!")
		io.new_line
	end

feature {NONE} -- Initialization
	make
	-- Initialization for `Current'.
	do end
end
