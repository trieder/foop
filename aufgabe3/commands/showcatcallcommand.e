note
	description: "demonstrates a cat-call"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SHOWCATCALLCOMMAND

inherit
	COMMAND

create
	make

feature

	Name : STRING
	do
		Result := "CATCALL"
	end

	execute
	--shows a catcall
	local
		game : GAME
		devgame : DEVGAME
		participation : PARTICIPATION
		player : GAMER
	do
		io.new_line
		print ("Creating a devgame, adding a normal participation, resulting in a catcall")
		io.new_line
		create devgame.make ("DevGame", 10, 5)
		game := devgame
		create player.make("Some", "Player")
		create participation.make(player)

		game.addparticipation (participation)
	end
feature {NONE}
	make
	do

	end
end
