note
	description: "List all the persons stored in the application."
	author: "Thomas Rieder"
	date: "$Date$"
	revision: "$Revision$"

class
	SHOWPERSONS

inherit
	COMMAND

create
	make

feature
	Name : STRING
	do
		Result := "SHOWPERSONS"
	end

	execute
	do
		io.new_line
		print ("The following people are in the store: ")
		io.new_line
		print("----------------------------")
		io.new_line
		across personstore.getpersons as person
	  	loop
	  		print (person.item.tostring)
	  		io.new_line
	  	end
		io.new_line
		print("----------------------------")
		io.new_line
	end

feature {NONE}
	-- stores the persons themselves
	personstore : PERSONSTORE

	make(pPersonstore : PERSONSTORE)
	do
		personstore := pPersonstore

	end

invariant
	store_not_void: personstore.getpersons /= VOID
	store_not_empty: not personstore.getpersons.is_empty
end
