note
	description: "command to show information about the game that was added last"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SHOWLASTGAMECOMMAND
inherit
	COMMAND

create
	make

feature
	Name : STRING
	do
		Result := "LASTGAME"
	end

	execute
	do
		io.new_line
		print(gamelog.lastgame.toString)
		io.new_line
	end

feature {NONE} -- Initialization
	gamelog : GAMELOG
	--the gamelog where the new created game is stored

	make (log :GAMELOG)
	do
		gamelog := log
	end

end
