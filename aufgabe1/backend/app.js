var http = require('http');
var finalhandler = require('finalhandler');
var serveStatic = require('serve-static');
var WebSocketServer = require('ws').Server;
var Map = require('./models/Map');
var Player = require('./models/Player');
var Mouse = require('./models/Mouse');
var Game = require('./models/Game');
var Wall = require('./models/Wall');
var Cheese = require('./models/Cheese');
var Entry = require('./models/Entry');
var Wind = require('./models/Wind');
var StrategyFactory = require('./models/Strategies');
var InputWind = require('./models/InputWind');
var PreGame = require('./models/PreGame');
var wss = new WebSocketServer({port: 8080});
var JoinGameHandler = require('./handlers/JoinGameHandler');
var StartGameHandler = require('./handlers/StartGameHandler');
var LeaveGameHandler = require('./handlers/LeaveGameHandler');
var CreateGameHandler = require('./handlers/CreateGameHandler');
var GameInputHandler = require('./handlers/GameInputHandler');

var serve = serveStatic('../frontend', {'index': ['index.html']});

var server = http.createServer(function (req, res) {
  var done = finalhandler(req, res);
  serve(req, res, done);
});

server.listen(3000);


var playercounter = 1;

//Global variables
gameidcounter = 0;
pregames = {};
playerDictionary = {};
playerDictionary.sendPregames = function () {
    for(var player in playerDictionary)
    {
        var player =  playerDictionary[player];
        //Check because we iterate also over broadcastall
        if (typeof(player) != "function") {
            player.sendPregames();
        }

    }
};

games = new Array();


//MessageHandlers
var messageHandlers = [];
messageHandlers.push( new GameInputHandler());
messageHandlers.push( new JoinGameHandler());
messageHandlers.push( new CreateGameHandler());
messageHandlers.push( new LeaveGameHandler());
messageHandlers.push( new StartGameHandler());


wss.on('connection', function connection(ws) {
    var player = new Player("player" + playercounter++, ws, null);
    playerDictionary[player.name] = player;


    player.sendConnectionSuccessfull();
    player.sendPlayerName();
    player.sendPregames();


  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
      var payload = JSON.parse(message);
      if (payload) {
          messageHandlers.forEach(function (handler) {
              handler.handle(payload, player);
          });
      }
  });
});

