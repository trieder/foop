/**
 * Created by Lisa on 20.05.2015.
 */

function Entry(x,y) {
  this.positionX = x;
  this.positionY = y;
  this.isFree = true;
}

module.exports = Entry;
