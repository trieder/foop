/**
 * Created by Lisa on 20.05.2015.
 */

function Mouse(color, x, y) {
  this.color = color;
  this.positionX = x;
  this.positionY = y;
  this.direction = '';
  this.pause = false;
}

Mouse.prototype.setPosition = function(x,y) {
  this.positionX = x;
  this.positionY = y;
}

Mouse.prototype.GREY = 'g';
Mouse.prototype.BLUE = 'b';
Mouse.prototype.BRIGHTBLUE = 'c';
Mouse.prototype.BRIGHTGREEN = 'k';
Mouse.prototype.DARKGREEN = 'l';
Mouse.prototype.YELLOW = 'y';
Mouse.prototype.RED = 'r';
Mouse.prototype.ORANGE = 'o';
Mouse.prototype.VIOLET = 'v';

Mouse.prototype.NORTH = 'n';
Mouse.prototype.EAST = 'e';
Mouse.prototype.SOUTH = 's';
Mouse.prototype.WEST = 'w';

module.exports = Mouse;