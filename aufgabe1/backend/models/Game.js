/**
 * Created by Lisa on 20.05.2015.
 */
var Player = require('./Player');
var Wall = require('./Wall');
var Map = require('./Map');
var Cheese = require('./Cheese');
var Entry = require('./Entry');
var Mouse = require('./Mouse');
var Wind = require('./Wind');
var StrategyFactory = require('./Strategies');
var InputWind = require('./InputWind');

function Game(id, level) {
  this.id = id;
  this.players = new Array();      // array of player
  this.map = new Map();
  this.map.currentLevel = level;
  this.currentLevel = this.map.currentLevel;
  this.walls = new Array();      // array of wall
  this.cheese = undefined;
  this.entries = new Array();
  this.wind = new Wind();
  this.playerInputs = new Array();
  this.freeMouseColors = [ Mouse.prototype.ORANGE, Mouse.prototype.VIOLET, Mouse.prototype.RED, Mouse.prototype.BRIGHTBLUE,
    Mouse.prototype.DARKGREEN, Mouse.prototype.YELLOW, Mouse.prototype.BRIGHTGREEN,
    Mouse.prototype.GREY, Mouse.prototype.BLUE];
  this.levelFinished = false;
  this.levelWinner = undefined;
  this.isFinished = false;
  this.nextLevel = 0;


  this.setWallsAndCheese();

  var that = this;

  var randomizeWind = function() {
    var directions = [ Wind.prototype.NORTH, Wind.prototype.EAST, Wind.prototype.SOUTH, Wind.prototype.WEST];
    var index = Math.floor(Math.random() * directions.length);
    var randomWind = new InputWind(directions[index]);

    that.players.forEach(function (player) {
      player.windInputs.push(randomWind);
      var response = player.getWindResponse();
      player.sendResponse(response);
    });
  };

  var intervalRandomWind = setInterval( randomizeWind, 1000);

  var intervalMouseRuns = setInterval(function () {
    console.log('try interval');
    if(that.players.length > 0) {


      console.log('interval - update map');
      var fact = new StrategyFactory();

      var wind = new Wind();

      // sum up wind inputs of all players
      that.players.forEach(function (player) {
        var playerWind = player.takeWind();
        wind.north = wind.north + playerWind.north;
        wind.east = wind.east + playerWind.east;
        wind.south = wind.south + playerWind.south;
        wind.west = wind.west + playerWind.west;
      })

      var windDirection = new Array();
      var highestWind = { type: undefined, value: 0.0};
      if (wind.north >= wind.south) {
        windDirection.push(Wind.prototype.NORTH);
        highestWind.type = Wind.prototype.NORTH;
        highestWind.value = wind.north;
      } else {
        windDirection.push(Wind.prototype.SOUTH);
        highestWind.type = Wind.prototype.SOUTH;
        highestWind.value = wind.south;
      }

      if (wind.east >= wind.west) {
        windDirection.push(Wind.prototype.EAST);
        highestWind.type = Wind.prototype.EAST;
        highestWind.value = wind.east;
      } else {
        windDirection.push(Wind.prototype.WEST);
        highestWind.type = Wind.prototype.WEST;
        highestWind.value = wind.west;
      }

      // drive only if wind exists
      if (windDirection.length > 0) {

        // each mouse run - set mouse position
        that.players.forEach(function (player) {

          var mouse = player.mouse;
          var index = 0;

          var possibleMouseDirections = new Array();

          // wind impact to mouse run
          if (windDirection.indexOf(Wind.prototype.NORTH) > -1) {
            possibleMouseDirections.push(Mouse.prototype.SOUTH);
          }
          if (windDirection.indexOf(Wind.prototype.SOUTH) > -1) {
            possibleMouseDirections.push(Mouse.prototype.NORTH);
          }
          if (windDirection.indexOf(Wind.prototype.EAST) > -1) {
            possibleMouseDirections.push(Mouse.prototype.WEST);
          }
          if (windDirection.indexOf(Wind.prototype.WEST) > -1) {
            possibleMouseDirections.push(Mouse.prototype.EAST);
          }

          possibleMouseDirections = checkDirectionBorder(possibleMouseDirections, mouse);
          possibleMouseDirections = checkDirectionWall(possibleMouseDirections, mouse);

          if (possibleMouseDirections.length > 0 && player.mouse.pause == false) {

            // take highest wind
            index = possibleMouseDirections.indexOf(highestWind.type);
            if (index == -1) {
              index = Math.floor(Math.random() * possibleMouseDirections.length);
            }
            console.log(mouse.color + ": index " + index + "  wind " + possibleMouseDirections[index]);

          } else {
            // all directions are possible - does not consider highest wind inputs
            possibleMouseDirections = [ Mouse.prototype.NORTH, Mouse.prototype.EAST, Mouse.prototype.SOUTH, Mouse.prototype.WEST];
            possibleMouseDirections = checkDirectionBorder(possibleMouseDirections, mouse);
            possibleMouseDirections = checkDirectionWall(possibleMouseDirections, mouse);

            // if mouse had to wait in former run - run into another direction than before
            if (player.mouse.pause) {
              possibleMouseDirections = removeFromArray(possibleMouseDirections, mouse.direction);
              player.mouse.pause = false;
            }

            index = Math.floor(Math.random() * possibleMouseDirections.length);
            console.log(mouse.color + ": random " + possibleMouseDirections[index]);
          }

          if(possibleMouseDirections.length > 0) {
            // mouse run in direction
            player.setStrategy(fact.createStrategy(possibleMouseDirections[index]));
            // remove mouse from map - try to run, if not possible e.g. another mouse on this place - mouse has to pause
            that.map.removeMouse(player.mouse);
            player.run();
            var success = that.map.setMouse(player.mouse);

            if (!success) {
              that.map.removeMouse(player.mouse);
              player.runBack();
              player.mouse.pause = true;
              that.map.setMouse(player.mouse);
            } else {
              // check if cheese is reached
              if(mouse.positionX == that.cheese.positionX && mouse.positionY == that.cheese.positionY) {
                that.levelFinished = true;
                that.levelWinner = mouse.color;
                that.nextLevel = that.map.currentLevel + 1;
              }
            }
          }

        });

        that.broadcastMap();

        if(that.levelFinished) {
          that.broadcast({
            type: "level_finished",
            level: that.map.currentLevel,
            winner: that.levelWinner
          });

          if(that.map.currentLevel < that.map.maxLevel) {
            that.startNextLevel();
          } else {
            //game is over
            clearInterval(intervalRandomWind);
            clearInterval(intervalMouseRuns);
            that.isFinished = true;

            setTimeout(that.broadcast({
              type: "wind_updated",
              north: 0.0,
              east: 0.0,
              south: 0.0,
              west: 0.0
            }),500);
            setTimeout(that.broadcast({
              type: "game_over"
            }), 1000);
          }

        }

      }
    }

  }, 2000);

  function removeFromArray(array, element) {
    var index = array.indexOf(element);
    if(index > -1) {
      array.splice(index, 1);
    }
    return array;
  }

  function checkDirectionBorder(array, mouse) {
    if (mouse.positionY < 1) {
      array = removeFromArray(array, Mouse.prototype.NORTH);
    }
    if (mouse.positionY >= that.map.height()-1) {
      array = removeFromArray(array, Mouse.prototype.SOUTH);
    }
    if (mouse.positionX < 1) {
      array = removeFromArray(array, Mouse.prototype.WEST);
    }
    if (mouse.positionX >= that.map.width()-1) {
      array = removeFromArray(array, Mouse.prototype.EAST);
    }
    return array;
  }

  function checkDirectionWall(array, mouse) {
    var retArray = array;
    that.walls.forEach(function (wall) {
      if ((mouse.positionY - 1 == wall.positionY) && (mouse.positionX == wall.positionX)) {
        // north
        retArray = removeFromArray(retArray, Mouse.prototype.NORTH);
      } else if ((mouse.positionY + 1 == wall.positionY) && (mouse.positionX == wall.positionX)) {
        // south
        retArray = removeFromArray(retArray, Mouse.prototype.SOUTH);
      } else if ((mouse.positionX + 1 == wall.positionX) && (mouse.positionY == wall.positionY)) {
        // east
        retArray = removeFromArray(retArray, Mouse.prototype.EAST);
      } else if ((mouse.positionX - 1 == wall.positionX) && (mouse.positionY == wall.positionY)) {
        // west
        retArray = removeFromArray(retArray, Mouse.prototype.WEST);
      }
    });
    return retArray;
  }

}

Game.prototype.startNextLevel = function() {
  if(this.map.currentLevel < this.map.maxLevel) {
    this.map.currentLevel = this.map.currentLevel + 1;
    this.setWallsAndCheese();
    this.setMousesOnEntries();

    this.levelFinished = false;
    this.levelWinner = undefined;

    this.broadcastMap();
  }
}

Game.prototype.setWallsAndCheese = function() {
  var map = this.map.getLevel();
  this.walls = new Array();
  this.entries = new Array();
  this.cheese = undefined;

  for(var y = 0; y < map.length; y++) {
    var line = map[y];
    for(var x = 0; x < line.length; x++) {
      var data = line[x];

      switch(data) {
        case '*':
          var wall = new Wall(x,y);
          this.walls.push(wall);
          break;
        case '+':
          this.cheese = new Cheese(x,y);
          break;
        case ' ':
          if(y == 0 || y == (map.length - 1) || x == 0 || x == (line.length - 1)) {
            var entry = new Entry(x,y);
            this.entries.push(entry);
          }
        default:
          break;

      }
    }

  }
}

Game.prototype.setMousesOnEntries = function() {
  var that = this;

  this.players.forEach(function (player ){
    var freeEntries = that.getFreeEntries();
    if (freeEntries) {
      var e = freeEntries[0];
      var mouse = player.mouse;
      mouse.setPosition(e.positionX, e.positionY);
      that.setEntryUsed(e);

      if (mouse.positionY == 0) {
        // top wall
        mouse.direction = Mouse.prototype.SOUTH;
      }
      if (mouse.positionY == that.map.height() - 1) {
        // bottom wall
        mouse.direction = Mouse.prototype.NORTH;
      }
      if (mouse.positionX == 0) {
        // left wall
        mouse.direction = Mouse.prototype.EAST;
      }
      if (mouse.positionX == that.map.width() - 1) {
        // right wall
        mouse.direction = Mouse.prototype.WEST;
      }

      var success = that.map.setMouse(mouse);
      console.log("game set mouse " + success);
    }
  })
}

Game.prototype.getFreeEntries = function() {
  var entries = this.entries.filter(function(entry) {
    return entry.isFree;
  });
  return entries;
}


Game.prototype.setEntryUsed = function(entry) {
  this.entries.forEach(function(e){
    if(e.positionX == entry.positionX && e.positionY == entry.positionY) {
      e.isFree = false;
    }
  });
}

Game.prototype.broadcast = function (data) {
  this.players.forEach(function (player) {
    player.sendResponse(data);
  })
}

Game.prototype.addPlayer  =function(player) {
  var freeEntries = this.getFreeEntries();
  if(freeEntries.length > 0) {
    var color = this.freeMouseColors.pop();
    if(color) {
      var e = freeEntries[0];
      var mouse = new Mouse(color, e.positionX, e.positionY);
      this.setEntryUsed(e);

      if(mouse.positionY == 0) {
        // top wall
        mouse.direction = Mouse.prototype.SOUTH;
      }

      if(mouse.positionY == this.map.height() - 1) {
        // bottom wall
        mouse.direction = Mouse.prototype.NORTH;
      }

      if(mouse.positionX == 0) {
        // left wall
        mouse.direction = Mouse.prototype.EAST;
      }
      if(mouse.positionX == this.map.width() - 1) {
        // right wall
        mouse.direction = Mouse.prototype.WEST;
      }

      var success = this.map.setMouse(mouse);
      console.log("game set mouse " + success);

      player.mouse = mouse;
      player.game = this;
      this.players.push(player);
      console.log('successfully added player to game');
    }

  }
}

Game.prototype.broadcastColors = function()
{
  var names = new Array();
  this.players.forEach(function (player) {
    names.push({  "name": player.name, "color": player.mouse.color})
  });
  var msg = {
    "type":"playercolors",
    "colors":names};
  this.broadcast(msg);
}

Game.prototype.broadcastMap = function() {
  this.broadcast({
    type: "map_updated",
    map: this.map.getLevel(),
    level: this.map.currentLevel
  })
}

module.exports = Game;