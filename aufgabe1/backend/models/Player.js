/**
 * Created by Lisa on 20.05.2015.
 */

var Wind = require('./Wind');
var InputWind = require('./InputWind');

function Player(name, ws, game) {
  this.ws = ws
  this.name = name;
  this.mouse = {};
  this.strategy = "";
  this.game = game;
  this.windInputs = new Array();

}

Player.prototype =  {
  setStrategy: function(strategy) {
    this.strategy = strategy;
  },

  run: function() {
    return this.strategy.run(this.mouse);
  },

  runBack: function() {
    return this.strategy.runBack(this.mouse);
  }
}

Player.prototype.getWindResponse = function() {
  var response = { type: "wind_updated"};
  var wind = this.processWindResponse(this.windInputs);
  response.north = wind.north;
  response.east = wind.east;
  response.south = wind.south;
  response.west = wind.west;

  return response;

}

Player.prototype.takeWind = function() {
  var copy = this.windInputs;
  this.windInputs = new Array();
  return this.processWindResponse(copy);
};

Player.prototype.processWindResponse = function(array) {
  var date = new Date();
  var wind = new Wind();
  array.forEach(function (inputWind) {
    if( (date.getMilliseconds() - 4000) <= inputWind.timestamp) {
      switch (inputWind.direction) {
        case Wind.prototype.NORTH:
          wind.north = wind.north + 1;
          break;
        case Wind.prototype.EAST:
          wind.east = wind.east + 1;
          break;
        case Wind.prototype.SOUTH:
          wind.south = wind.south + 1;
          break;
        case Wind.prototype.WEST:
          wind.west = wind.west + 1;
          break;
        default:
          break;
      }
    }
  });


  // limit wind direction with 100 clicks in last 4 seconds
    wind.north = wind.north / 10 < 1 ? wind.north / 10 : 1;
    wind.east = wind.east / 10 < 1 ? wind.east / 10 : 1;
    wind.south = wind.south / 10 < 1 ? wind.south / 10 : 1;
    wind.west = wind.west / 10 < 1 ? wind.west / 10 : 1;
    return wind;

}

Player.prototype.sendConnectionSuccessfull = function () {
    this.sendResponse({
        type: "connection_successful",
        date: new Date()
    });
}

Player.prototype.sendPlayerName = function () {
    this.sendResponse({
        type: "playerinfo",
            name: this.name
    });
}

Player.prototype.sendPregames = function () {
    this.sendResponse({
        type: "pregames",
            pregames: pregames

    });
}

Player.prototype.sendResponse = function(response) {
  if (this.ws.readyState == this.ws.OPEN) {
    this.ws.send(JSON.stringify(response));
  }
}

Player.prototype.sendError = function(errormsg) {
    this.sendResponse({
        type: "errormessage",
        message: errormsg
    });
}

module.exports = Player;