/**
 * Created by Lisa on 20.05.2015.
 */

var Mouse = require('./Mouse');

function North() {
  this.run = function(mouse) {
    mouse.positionY = mouse.positionY - 1;
    mouse.direction = Mouse.prototype.NORTH;
  };

  this.runBack = function(mouse) {
    mouse.positionY = mouse.positionY + 1;
  };
}

function East() {
  this.run = function(mouse) {
    mouse.positionX = mouse.positionX + 1;
    mouse.direction = Mouse.prototype.EAST;
  };

  this.runBack = function(mouse) {
    mouse.positionX = mouse.positionX - 1;
  };
}

function South() {
  this.run = function(mouse) {
    mouse.positionY = mouse.positionY + 1;
    mouse.direction = Mouse.prototype.SOUTH;
  };

  this.runBack = function(mouse) {
    mouse.positionY = mouse.positionY - 1;
  };
}

function West() {
  this.run = function(mouse) {
    mouse.positionX = mouse.positionX - 1;
    mouse.direction = Mouse.prototype.WEST;
  };

  this.runBack = function(mouse) {
    mouse.positionX = mouse.positionX + 1;
  };
}



function StrategyFactory() {
  this.north = '';
  this.east = '';
  this.south = '';
  this.west = '';
}

StrategyFactory.prototype.createStrategy = function(strategy) {
  switch(strategy) {
    case Mouse.prototype.NORTH:
      if(!this.north) {
        this.north = new North();
      }
      return this.north;

    case Mouse.prototype.EAST:
      if(!this.east) {
        this.east = new East();
      }
      return this.east;
    case Mouse.prototype.SOUTH:
      if(!this.south) {
        this.south = new South();
      }
      return this.south;
    case Mouse.prototype.WEST:
      if(!this.west) {
        this.west = new West();
      }
      return this.west;
    default:
      break;
  }
}

module.exports = StrategyFactory;