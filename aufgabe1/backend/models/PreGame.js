/**
 * Created by patrick on 27.05.2015.
 */
var Game = require('./Game');

function PreGame(name,id,creatorname) {
    this.name = name;
    this.id = id;
    this.creator = creatorname;
    this.playernames = new Array();
    this.playernames.push(creatorname);
}


PreGame.prototype.addPlayer = function(p)
{
    this.playernames.push(p.name);
    p.sendResponse({
        type: "game_join",
        gameid : this.id
    });
}

PreGame.prototype.removePlayer = function(p)
{
    var index = this.playernames.indexOf(p.name);
    if (index > -1) {
        this.playernames.splice(index, 1);
    }
    p.sendResponse({
        type: "game_leave"
    });
}

PreGame.prototype.notifyCreator = function (playerdictionary) {
    playerdictionary[this.creator].sendResponse( {type: "game_join",
        gameid : this.id,
        jointype: "creator"});
}


PreGame.prototype.broadcastGameInfo = function (playerdictionary) {
    var msg = {
             type: "pregameinfo",
             game: this
         };
    this.broadcast(playerdictionary,msg);
}

PreGame.prototype.broadcastGameStart = function (playerdictionary) {
    var msg = {
        type: "gamestarted"
    };
    this.broadcast(playerdictionary,msg);
}


PreGame.prototype.broadcast = function (playerdictionary,message)
{
    for(var p in this.playernames)
    {
        var player = playerdictionary[this.playernames[p]];
        player.sendResponse(message);
    }
}

PreGame.prototype.createGame = function(playerdictionary,gameid)
{
    var g = new Game(gameid,0);
    for(var p in this.playernames)
    {
        var player = playerdictionary[this.playernames[p]];
        g.addPlayer(player);
    }
    g.broadcastColors();
    return g;
}


PreGame.prototype.broadcastGameClosed = function(playerdictionary)
{
    var message = {
        type: "gameclosed",
        game: this
    }
    this.broadcast(playerdictionary,message);
}


module.exports =  PreGame;