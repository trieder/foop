/**
 * Created by Lisa on 20.05.2015.
 */

function Wind() {
  this.north = 0.0;
  this.east = 0.0;
  this.south = 0.0;
  this.west = 0.0;
}

Wind.prototype.clear = function() {
  this.north = 0.0;
  this.east = 0.0;
  this.south = 0.0;
  this.west = 0.0;

}

Wind.prototype.NORTH = 'NORTHWIND';
Wind.prototype.EAST = 'EASTWIND';
Wind.prototype.SOUTH = 'SOUTHWIND';
Wind.prototype.WEST = 'WESTWIND';

module.exports = Wind;