/**
 * Created by patrick on 29.05.2015.
 */
var BaseHandler = require("./BaseHandler");
var PreGame = require("../models/PreGame");


function CreateGameHandler() {
    BaseHandler.call(this, "create_game");
}

CreateGameHandler.prototype = Object.create(BaseHandler.prototype);

CreateGameHandler.prototype.handlePayload =
    function (payload, player) {
        var gameid = gameidcounter++;
        var newGame = new PreGame("Game "+gameid,gameid,player.name);

        pregames[newGame.id]=newGame;
        playerDictionary.sendPregames();

        newGame.notifyCreator(playerDictionary);
        newGame.broadcastGameInfo(playerDictionary);
    };

CreateGameHandler.prototype.constructor = CreateGameHandler;

module.exports = CreateGameHandler;