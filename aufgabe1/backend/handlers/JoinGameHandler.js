/**
 * Created by patrick on 29.05.2015.
 */
var BaseHandler = require("./BaseHandler");

function JoinGameHandler() {
    BaseHandler.call(this, "joingame");
}

JoinGameHandler.prototype = Object.create(BaseHandler.prototype);

JoinGameHandler.prototype.handlePayload =
    function (payload, player) {
        var game = pregames[payload.game];
        if(!game)
        {
            player.sendError("Game not found for game: "+payload.game);
        }
        else
        {
            game.addPlayer(player);
            game.broadcastGameInfo(playerDictionary);
        }
    };

JoinGameHandler.prototype.constructor = JoinGameHandler;
module.exports = JoinGameHandler;