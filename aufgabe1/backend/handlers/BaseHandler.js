/**
 * Created by patrick on 29.05.2015.
 */
function BaseHandler(typeToHandle) {
    this.typeToHandle = typeToHandle;
}

BaseHandler.prototype = {
    handle: function(payload,player) {
        if(this.typeToHandle == payload.type)
        {
            this.handlePayload(payload,player);
        }
    },

    handlePayload: function(payload,player) {
        throw "HandlePayload Not Implemented";
    }

};

BaseHandler.prototype.constructor = BaseHandler;

module.exports = BaseHandler;
