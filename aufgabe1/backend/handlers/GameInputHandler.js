/**
 * Created by patrick on 29.05.2015.
 */
var BaseHandler = require("./BaseHandler");
var InputWind = require("../models/InputWind");
var Wind = require("../models/Wind");


function GameInputHandler() {
    BaseHandler.call(this, "game_input");
}

GameInputHandler.prototype = Object.create(BaseHandler.prototype);

GameInputHandler.prototype.handlePayload =
    function (payload, player) {
        if (player.game.isFinished == false && payload.direction) {
            switch (payload.direction) {
                case "north":
                    player.windInputs.push(new InputWind(Wind.prototype.NORTH));
                    break;
                case "east":
                    player.windInputs.push(new InputWind(Wind.prototype.EAST));
                    break;
                case "west":
                    player.windInputs.push(new InputWind(Wind.prototype.WEST));
                    break;
                case "south":
                    player.windInputs.push(new InputWind(Wind.prototype.SOUTH));
                    break;
            }

            setTimeout(player.sendResponse(player.getWindResponse()), 500);
        }

    };

GameInputHandler.prototype.constructor = GameInputHandler;

module.exports = GameInputHandler;