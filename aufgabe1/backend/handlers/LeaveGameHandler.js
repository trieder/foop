/**
 * Created by patrick on 29.05.2015.
 */
var BaseHandler = require("./BaseHandler");

function LeaveGameHandler() {
    BaseHandler.call(this, "leavegame");
}

LeaveGameHandler.prototype = Object.create(BaseHandler.prototype);

LeaveGameHandler.prototype.handlePayload =
    function (payload, player) {

        var pregame = pregames[payload.game];
        if (!pregame) {
            player.sendError("Game not found for game: " + payload.game);
        }
        else {
            //Check if i am the creatog
            if(pregame.creator == player.name)
            {
                //Game will be canceled
                pregame.broadcastGameClosed(playerDictionary);
                delete pregames[pregame.id];
                playerDictionary.sendPregames();
            }
            else
            {
                pregame.removePlayer(player);
                pregame.broadcastGameInfo(playerDictionary);
            }

        }
    };

LeaveGameHandler.prototype.constructor = LeaveGameHandler;
module.exports = LeaveGameHandler;




module.exports = LeaveGameHandler;