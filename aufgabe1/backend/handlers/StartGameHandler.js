/**
 * Created by patrick on 29.05.2015.
 */
var BaseHandler = require("./BaseHandler");

function StartGameHandler() {
    BaseHandler.call(this, "startgame");
}

StartGameHandler.prototype = Object.create(BaseHandler.prototype);

StartGameHandler.prototype.handlePayload =
    function (payload, player) {

        var pregame = pregames[payload.game];
        if (!pregame) {
            player.sendError( "Game not found for game: " + payload.game);
        }
        else {
            delete pregames[pregame.id];
            playerDictionary.sendPregames();
            pregame.broadcastGameStart(playerDictionary);

            var actualGame =  pregame.createGame(playerDictionary,pregame.id);
            actualGame.broadcastMap();
            games.push(actualGame);
        }
    };

StartGameHandler.prototype.constructor = StartGameHandler;
module.exports = StartGameHandler;
