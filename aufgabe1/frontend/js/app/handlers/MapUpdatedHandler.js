/**
 * Created by Thomas on 12.04.2015.
 */
define('handlers/MapUpdatedHandler', [ '../models/Map' ], function (Map) {

  function MapUpdatedHandler() {
    this.sizeX = 0;
    this.sizeY = 0;
    this.map = undefined;
  }

  MapUpdatedHandler.prototype.handle = function (payload) {
   if ("map_updated" === payload.type) {

      var data = payload.map;
      var level = payload.level;

     this.sizeY = data.length;
     this.sizeX = data[0].length;
     this.map = new Map(this.sizeX, this.sizeY);

      for (var i = 0; i < data.length; i++) {
        var row = data[i];
        for (var j = 0; j < row.length; j++) {
          var type;
          var direction;
          switch (row[j].substring(0,1)) {
            case "*":
              type = Map.prototype.WALL;
              direction = undefined;
              break;
            case " ":
              type = Map.prototype.NOTHING;
              direction = undefined;
              break;
            case "+":
              if(row[j].length > 1) {
                type = getCheeseColor(row[j].substring(2,row[j].length));

              } else {
                type = Map.prototype.CHEESE;
              }
              direction = undefined;
              break;
            case 'b':
              type = Map.prototype.MOUSEBLUE;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'c':
              type = Map.prototype.MOUSEBRIGHTBLUE;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'k':
              type = Map.prototype.MOUSEBRIGHTGREEN;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'l':
              type = Map.prototype.MOUSEDARKGREEN;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'g':
              type = Map.prototype.MOUSEGREY;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'o':
              type = Map.prototype.MOUSEORANGE;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'y':
              type = Map.prototype.MOUSEYELLOW;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'v':
              type = Map.prototype.MOUSEVIOLET;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
            case 'r':
              type = Map.prototype.MOUSERED;
              direction = getDirection(row[j].substring(2,row[j].length));
              break;
          }

          this.map.setPosition(j,i, type, direction);
        }
      }

      $("#map").find("> table > tbody").html('').append(this.map.generateHTML());
      $("#level-number").text(level);
    }
  };

  function getDirection(data) {
    switch (data) {
      case "n":
        return Map.prototype.MOUSENORTH;
      case "e":
        return Map.prototype.MOUSEEAST;
      case "s":
        return Map.prototype.MOUSESOUTH;
      case "w":
        return Map.prototype.MOUSEWEST;
      default:
        return Map.prototype.MOUSENORTH;
    }
  }

  function getCheeseColor(data) {
    switch (data) {
      case 'b':
        return Map.prototype.MOUSEBLUE_CHEESE;
      case 'c':
        return Map.prototype.MOUSEBRIGHTBLUE_CHEESE;
      case 'k':
        return Map.prototype.MOUSEBRIGHTGREEN_CHEESE;
      case 'l':
        return Map.prototype.MOUSEDARKGREEN_CHEESE;
      case 'g':
        return Map.prototype.MOUSEGREY_CHEESE;
      case 'o':
        return Map.prototype.MOUSEORANGE_CHEESE;
      case 'y':
        return Map.prototype.MOUSEYELLOW_CHEESE;
      case 'v':
        return Map.prototype.MOUSEVIOLET_CHEESE;
      case 'r':
        return Map.prototype.MOUSERED_CHEESE;
    }
  }

  return MapUpdatedHandler;
});

