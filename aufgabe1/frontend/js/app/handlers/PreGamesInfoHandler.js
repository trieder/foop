/**
 * Created by patrick on 27.05.2015.
 */

define('handlers/PreGamesInfoHandler', function() {

    // constructor
    function PreGamesInfoHandler(socket) {
        this.socket = socket;
    }

    PreGamesInfoHandler.prototype.handle = function (payload) {
        if ("pregames" === payload.type) {
            var pregames = payload.pregames;

            var html = "";
            for(var game in pregames)
            {
                html += '<option value="' + pregames[game].id+ '">' + pregames[game].name + '</option>';
            };

            $('#games').html('').append(html);

        }
    };


    // returns the constructor
    return PreGamesInfoHandler;
});
