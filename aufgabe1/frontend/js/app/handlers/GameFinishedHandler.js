/**
 * Created by Lisa on 27.05.2015.
 */
define('handlers/GameFinishedHandler', function() {

  //constructor
  function GameFinishedHandler() {}

  // methods
  GameFinishedHandler.prototype.handle = function(payload) {

    if("game_over" === payload.type) {
      $('#creategame').prop("disabled", false);
      $('#joingame').prop("disabled", false);
      gameStarted = false;
      $('#pregame').show();
    }
  }


  return GameFinishedHandler;
  });
