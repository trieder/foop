/**
 * Created by Thomas on 12.04.2015.
 */
define('handlers/GameStartedHandler', function() {

    // constructor
    function GameStartedHandler() {
    }

    // methods
    GameStartedHandler.prototype.handle = function(payload) {

        if ("gamestarted" === payload.type) {
          //game started ... so set leave and start disabled
            $( "#startgame").prop("disabled",true);
            $( "#leavegame").prop("disabled",true);
          gameStarted = true;
          $('#score').find('> table > tbody').html('');
          $('#pregame').hide();
          $('#activegame').show();
        }

    };

    // returns the constructor
    return GameStartedHandler;
});