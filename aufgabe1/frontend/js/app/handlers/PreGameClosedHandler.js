/**
 * Created by patrick on 27.05.2015.
 */
define('handlers/PreGameClosedHandler', function() {

    // constructor
    function PreGameClosedHandler() {
    }

    PreGameClosedHandler.prototype.handle = function (payload) {
        if ("gameclosed" === payload.type
            || "game_leave" == payload.type) //PS: two different scenarios on the server, same here

        {
            console.log("game-closed");

            $( "#creategame" ).prop( "disabled", false );
            $( "#joingame" ).prop( "disabled", false );

            $( "#startgame").prop("disabled",true);
            $( "#leavegame").prop("disabled",true);

            $( "#current_gamename").html('').append("Current Game: ");

            $("#players").empty();
        }
    };


    // returns the constructor
    return PreGameClosedHandler;
});