/**
 * Created by patrick on 27.05.2015.
 */
define('handlers/PreGameJoinedHandler', function() {

    // constructor
    function PreGameJoinedHandler() {
    }

    PreGameJoinedHandler.prototype.handle = function (payload) {
        if ("game_join" === payload.type) {
            console.log("joining game");
            $( "#creategame" ).prop( "disabled", true );
            $( "#joingame" ).prop( "disabled", true );
            if("creator" == payload.jointype)
            {
                $( "#startgame").prop("disabled",false);
                $( "#startgame").prop("value",payload.gameid);
            }
            $( "#leavegame").prop("disabled",false);
            $( "#leavegame").prop("value",payload.gameid);

        }
    };


    // returns the constructor
    return PreGameJoinedHandler;
});