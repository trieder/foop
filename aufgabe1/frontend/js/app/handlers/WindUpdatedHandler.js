/**
 * Created by Thomas on 12.04.2015.
 */
/**
 * Created by Thomas on 12.04.2015.
 */
define('handlers/WindUpdatedHandler', function (Wind) {

  function WindUpdatedHandler() {
  }

  WindUpdatedHandler.prototype.handle = function (payload) {
    if ("wind_updated" === payload.type) {

      if (payload.north != null) {
        $("#wind-status-north").find("> div").css("width", Math.ceil(payload.north * 100));
      }

      if (payload.west != null) {
        $("#wind-status-west").find("> div").css("width", Math.ceil(payload.west * 100));
      }

      if (payload.east != null) {
        $("#wind-status-east").find("> div").css("width", Math.ceil(payload.east * 100));
      }

      if (payload.south != null) {
        $("#wind-status-south").find("> div").css("width", Math.ceil(payload.south * 100));
      }
    }
  };

  return WindUpdatedHandler;
});