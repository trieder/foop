/**
 * Created by patrick on 27.05.2015.
 */

/**
 * Created by patrick on 27.05.2015.
 */

define('handlers/LeaveGameHandler', function() {

    // constructor
    function LeaveGameHandler(socket) {
        this.socket = socket;
    }

    // methods
    LeaveGameHandler.prototype.bind = function(selector) {

        var that = this;
        var sendEvent = function () {
            var selectedGame = $( "#leavegame" ).val();

            if(selectedGame==null) {
                alert("Error, leavegame had no value set");
                return;
            }
            console.log("Leaving: "+selectedGame);

           that.socket.send(JSON.stringify({
                type: "leavegame",
                game: selectedGame
            }));
        };
        $(selector).on("click", sendEvent);


    };

    // returns the constructor
    return LeaveGameHandler;
});