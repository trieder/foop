/**
 * Created by patrick on 27.05.2015.
 */
define('handlers/CreateGameHandler', function() {

    // constructor
    function CreateGameHandler(socket) {
        this.socket = socket;
    }

    // methods
    CreateGameHandler.prototype.bind = function(selector) {


        var that = this;
        var sendEvent = function () {
            that.socket.send(JSON.stringify({
                type: "create_game"
            }));
        };
        $(selector).on("click", sendEvent);


    };

    // returns the constructor
    return CreateGameHandler;
});