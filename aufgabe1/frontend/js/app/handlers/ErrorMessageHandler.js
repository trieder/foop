/**
 * Created by Thomas on 12.04.2015.
 */
define('handlers/ErrorMessageHandler', function() {

  // constructor
  function ErrorMessageHandler() {
  }

  // methods
    ErrorMessageHandler.prototype.handle = function(payload) {

        if ("errormessage" === payload.type) {
            var msg = payload.message;
            alert("ERROR: " + msg);
        }

  };

  // returns the constructor
  return ErrorMessageHandler;
});