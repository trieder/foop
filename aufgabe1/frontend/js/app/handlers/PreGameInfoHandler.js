/**
 * Created by patrick on 27.05.2015.
 */
define('handlers/PreGameInfoHandler', function() {

    // constructor
    function PreGameInfoHandler() {
    }

    PreGameInfoHandler.prototype.handle = function (payload) {
        if ("pregameinfo" === payload.type) {
            var game = payload.game;
            $( "#current_gamename").html('').append("Current Game: "+game.name);
            var players = "";
            var playernames = game.playernames;
            for(var id in playernames)
            {
                players +=  '<li class="list-group-item">'+ playernames[id]+ '</li>';
            };
            $("#players").html('').append(players);
        }
    };


    // returns the constructor
    return PreGameInfoHandler;
});