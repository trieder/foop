/**
 * Created by patrick on 27.05.2015.
 */

define('handlers/PlayerColorHandler',[ '../models/Map' ], function(Map) {

    // constructor
    function PlayerColorHandler() {
    }

    PlayerColorHandler.prototype.handle = function (payload) {
        if ("playercolors" === payload.type) {
            var colors = payload.colors;
            var html="";
            for(var c in colors)
            {
                var namecolor = colors[c];
                var colorname = getCheeseColor(namecolor.color);
               html +=  '<tr><td>'+namecolor.name +'</td><td id="mouse-'+colorname+'" class="map-mouse map-mouse-east"></td></tr>';
            }
            $('#playermouse').html('').append(html);
        }
    };


    function getCheeseColor(data) {
        switch (data) {
            case 'b':
                return Map.prototype.MOUSEBLUE;
            case 'c':
                return Map.prototype.MOUSEBRIGHTBLUE;
            case 'k':
                return Map.prototype.MOUSEBRIGHTGREEN;
            case 'l':
                return Map.prototype.MOUSEDARKGREEN;
            case 'g':
                return Map.prototype.MOUSEGREY;
            case 'o':
                return Map.prototype.MOUSEORANGE;
            case 'y':
                return Map.prototype.MOUSEYELLOW;
            case 'v':
                return Map.prototype.MOUSEVIOLET;
            case 'r':
                return Map.prototype.MOUSERED;
        }
    }

    // returns the constructor
    return PlayerColorHandler;
});
