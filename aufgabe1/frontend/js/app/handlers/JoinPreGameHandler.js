/**
 * Created by patrick on 27.05.2015.
 */

define('handlers/JoinPreGameHandler', function() {

    // constructor
    function JoinPreGameHandler(socket) {
        this.socket = socket;
    }

    // methods
    JoinPreGameHandler.prototype.bind = function(selector) {

        var that = this;
        var sendEvent = function () {
            var selectedGame = $( "#games" ).val();
            if(selectedGame==null) {
                alert("Please select a game");
                return;
            }
            console.log("Joining: "+selectedGame);

            that.socket.send(JSON.stringify({
                type: "joingame",
                game: selectedGame
            }));
        };
        $(selector).on("click", sendEvent);


    };

    // returns the constructor
    return JoinPreGameHandler;
});