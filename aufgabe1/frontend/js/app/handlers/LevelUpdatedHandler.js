/**
 * Created by Lisa on 26.05.2015.
 */

define('handlers/LevelUpdatedHandler', [ '../models/Map' ], function(Map) {

  // constructor
  function LevelUpdatedHandler() {
  }

  LevelUpdatedHandler.prototype.handle = function(payload) {
    if("level_finished" === payload.type) {
      console.log('level updated');

      var old_level = payload.level;
      var winner = payload.winner;
      var mouse = getMouse(winner);

      if (old_level != undefined && winner != undefined) {

        var html = '<tr><td>Level ' + old_level + '</td><td id=mouse-cheese-' + mouse + ' class="map-cheese"></td></tr>'

        $('#score').find('> table > tbody').append(html);


      }
    }

  }

  function getMouse(data) {
    switch (data) {
      case 'b':
        return Map.prototype.MOUSEBLUE;
      case 'c':
        return Map.prototype.MOUSEBRIGHTBLUE;
      case 'k':
        return Map.prototype.MOUSEBRIGHTGREEN;
      case 'l':
        return Map.prototype.MOUSEDARKGREEN;
      case 'g':
        return Map.prototype.MOUSEGREY;
      case 'o':
        return Map.prototype.MOUSEORANGE;
      case 'y':
        return Map.prototype.MOUSEYELLOW;
      case 'v':
        return Map.prototype.MOUSEVIOLET;
      case 'r':
        return Map.prototype.MOUSERED;
    }
  }

  return LevelUpdatedHandler;
});
