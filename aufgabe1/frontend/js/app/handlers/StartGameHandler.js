/**
 * Created by patrick on 27.05.2015.
 */

/**
 * Created by patrick on 27.05.2015.
 */

define('handlers/StartGameHandler', function() {

    // constructor
    function StartGameHandler(socket) {
        this.socket = socket;
    }

    // methods
    StartGameHandler.prototype.bind = function(selector) {

        var that = this;
        var sendEvent = function () {
            var selectedGame = $( "#startgame" ).val();

            if(selectedGame==null) {
                alert("Please select a game");
                return;
            }
            console.log("Starting: "+selectedGame);

           that.socket.send(JSON.stringify({
                type: "startgame",
                game: selectedGame
            }));
        };
        $(selector).on("click", sendEvent);


    };

    // returns the constructor
    return StartGameHandler;
});