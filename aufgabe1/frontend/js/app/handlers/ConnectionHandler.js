/**
 * Created by thomasrieder on 12.04.15.
 */
define('handlers/ConnectionHandler', function() {

  // constructor
  function ConnectionHandler(socket) {
    // constructor args could go here
    this.socket = socket;
    monitorConnection(socket);
  }

  // methods
  ConnectionHandler.prototype.handle = function(payload) {
    if ("connection_successful" === payload.type) {
      $("#connection-status-symbol").find("i").removeClass("fa-times").addClass("fa-check");
    }
  };

  var monitorConnection = function (socket) {
    var check = function () {
      /*
         0 - connection not yet established
         1 - conncetion established
         2 - in closing handshake
         3 - connection closed or could not open
       */
      if (socket.readyState > 1) {
        $("#connection-status-symbol").find("i").removeClass("fa-check").addClass("fa-times");
      }
      setTimeout(check, 500);
    };
    check();
  };

  // returns the constructor
  return ConnectionHandler;
});