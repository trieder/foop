/**
 * Created by Thomas on 12.04.2015.
 */
define('handlers/GameInputHandler', function() {

  // constructor
  function GameInputHandler(socket) {
    this.socket = socket;
  }

  // methods
  GameInputHandler.prototype.bind = function(selector, direction, keyshortcut) {

    var that = this;
    var sendEvent = function () {
      if(gameStarted) {
        that.socket.send(JSON.stringify({
          type: "game_input",
          direction: direction
        }));
      }

    };

    $(selector).on("click", sendEvent);
    $(document).bind('keydown', keyshortcut, sendEvent);

  };

  // returns the constructor
  return GameInputHandler;
});