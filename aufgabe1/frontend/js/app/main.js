/**
 * Created by thomasrieder on 12.04.15.
 */
define(

  // name of this module
  "main",

  // dependencies
  [
    "handlers/ConnectionHandler",
    "handlers/MapUpdatedHandler",
    "handlers/WindUpdatedHandler",
    "handlers/GameInputHandler",
    "handlers/LevelUpdatedHandler",
    "handlers/PreGamesInfoHandler",
    "handlers/CreateGameHandler",
    "handlers/PreGameJoinedHandler",
    "handlers/PreGameInfoHandler",
    "handlers/PlayerInfoHandler",
    "handlers/JoinPreGameHandler",
    "handlers/ErrorMessageHandler",
    "handlers/StartGameHandler",
    "handlers/GameStartedHandler",
    "handlers/PlayerColorHandler",
    "handlers/GameFinishedHandler",
    "handlers/LeaveGameHandler",
    "handlers/PreGameClosedHandler"
  ],

  // dependencies get injected here
  function (ConnectionHandler, MapUpdatedHandler, WindUpdatedHandler,
            GameInputHandler, LevelUpdatedHandler,PreGamesInfoHandler,
            CreateGameHandler,PreGameJoinedHandler,PreGameInfoHandler,
            PlayerInfoHandler, JoinPreGameHandler,ErrorMessageHandler,
            StartGameHandler, GameStartedHandler, PlayerColorHandler,
            GameFinishedHandler,LeaveGameHandler, PreGameClosedHandler) {

    console.log("Yay I get loaded :-)");

    var webSocket = new WebSocket("ws://localhost:8080");
    // global variables
    gameStarted = false;


    //############ InputHandlers
    var gameInputHandler = new GameInputHandler(webSocket);
    gameInputHandler.bind("#game-control-north", "north", "up");
    gameInputHandler.bind("#game-control-east", "east", "right");
    gameInputHandler.bind("#game-control-west", "west", "left");
    gameInputHandler.bind("#game-control-south", "south", "down");

    var createGameHandler = new CreateGameHandler(webSocket);
    createGameHandler.bind("#creategame");

    var joinPreGameHandler =  new JoinPreGameHandler(webSocket);
    joinPreGameHandler.bind('#joingame');

    var startGameHandler = new StartGameHandler(webSocket);
    startGameHandler.bind("#startgame");

    var leaveGameHandler = new LeaveGameHandler(webSocket);
    leaveGameHandler.bind("#leavegame");


    //############ ConnectionHandlers
    var socketEventHandlers = [];
    socketEventHandlers.push(new ConnectionHandler(webSocket));
    socketEventHandlers.push(new LevelUpdatedHandler());
    socketEventHandlers.push(new MapUpdatedHandler());
    socketEventHandlers.push(new WindUpdatedHandler());
    socketEventHandlers.push(new PreGamesInfoHandler());
    socketEventHandlers.push(new PreGameJoinedHandler());
    socketEventHandlers.push(new PreGameInfoHandler());
    socketEventHandlers.push(new PlayerInfoHandler());
    socketEventHandlers.push(new ErrorMessageHandler());
    socketEventHandlers.push(new GameStartedHandler());
    socketEventHandlers.push(new PlayerColorHandler());
    socketEventHandlers.push(new GameFinishedHandler());
    socketEventHandlers.push(new PreGameClosedHandler());


    // main dispatch loop
    webSocket.onmessage = function (event) {
      if (event.data) {
        var payload = JSON.parse(event.data);
        if (payload) {
          socketEventHandlers.forEach(function (handler) {
            handler.handle(payload);
          });
        }
      }
    }
  });