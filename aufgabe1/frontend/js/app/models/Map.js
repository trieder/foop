/**
 * Created by Thomas on 12.04.2015.
 */
define('models/Map', function () {

  function Map(sizeX, sizeY) {
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    this.data = new Array(sizeY);
    this.directions = {};

    /* initialize the map array */
    for (var i = 0; i < sizeY; i++) {
      this.data[i] = new Array(sizeX);
    }
  }

  /* metadata may be stuff like the PLAYER ID */
  Map.prototype.setPosition = function (x, y, type, direction, metadata) {
    if (this.getAvailableCellTypes().indexOf(type) !== -1
      && x < this.sizeX
      && y < this.sizeY
      && x >= 0
      && y >= 0) {

      if(direction) {
        this.directions[type] = direction;
      }

      this.data[y][x] = type;
    } else {
      console.log("attempted to set position " + x + "/" + y + " to invalid type " + type);
    }
  };

  Map.prototype.generateHTML = function () {
    var ret = '';

    for (var i = 0; i < this.sizeY; i++) {
      ret += '<tr>';
      for (var j = 0; j < this.sizeX; j++) {
        ret += generateHTMLforCell(this.data, i, j, this.directions);
      }
      ret += '</tr>';
    }

    return ret;
  };

  /* private */
  var generateHTMLforCell = function (map, y, x, directions) {
    var data = map[y][x];

    switch (data) {
      case Map.prototype.WALL:
        return '<td class="map-wall"></td>';
        break;
      case Map.prototype.CHEESE:
        return '<td class="map-cheese"></td>';
        break;
      case Map.prototype.NOTHING:
        return '<td class="map-nothing"></td>';
        break;
      case Map.prototype.MOUSEGREY:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEBLUE:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEBRIGHTBLUE:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEBRIGHTGREEN:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEDARKGREEN:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEYELLOW:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSERED:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEORANGE:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEVIOLET:
        return '<td id=mouse-' + data + ' class="map-mouse map-mouse-' + directions[data] + '"></td>';
      case Map.prototype.MOUSEGREY_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEBLUE_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEBRIGHTBLUE_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEBRIGHTGREEN_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEDARKGREEN_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEYELLOW_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSERED_CHEESE:
      return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEORANGE_CHEESE:
      return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      case Map.prototype.MOUSEVIOLET_CHEESE:
        return '<td id=mouse-' + data + ' class="map-cheese"></td>';
      default:
        return '';
    }
  };

  Map.prototype.WALL = "TYPE_WALL";
  Map.prototype.PLAYER = "TYPE_PLAYER";
  Map.prototype.CHEESE = "TYPE_CHEESE";
  Map.prototype.NOTHING = "NOTHING";


  Map.prototype.MOUSENORTH = "north";
  Map.prototype.MOUSEEAST = "east";
  Map.prototype.MOUSESOUTH = "south";
  Map.prototype.MOUSEWEST = "west";

  Map.prototype.MOUSEGREY = 'grey';
  Map.prototype.MOUSEBLUE = 'blue';
  Map.prototype.MOUSEBRIGHTBLUE = 'bright-blue';
  Map.prototype.MOUSEBRIGHTGREEN = 'bright-green';
  Map.prototype.MOUSEDARKGREEN = 'dark-green';
  Map.prototype.MOUSEYELLOW = 'yellow';
  Map.prototype.MOUSERED = 'red';
  Map.prototype.MOUSEORANGE = 'orange';
  Map.prototype.MOUSEVIOLET = 'violet';

  Map.prototype.MOUSEGREY_CHEESE = 'cheese-grey';
  Map.prototype.MOUSEBLUE_CHEESE = 'cheese-blue';
  Map.prototype.MOUSEBRIGHTBLUE_CHEESE = 'cheese-bright-blue';
  Map.prototype.MOUSEBRIGHTGREEN_CHEESE = 'cheese-bright-green';
  Map.prototype.MOUSEDARKGREEN_CHEESE = 'cheese-dark-green';
  Map.prototype.MOUSEYELLOW_CHEESE = 'cheese-yellow';
  Map.prototype.MOUSERED_CHEESE = 'cheese-red';
  Map.prototype.MOUSEORANGE_CHEESE = 'cheese-orange';
  Map.prototype.MOUSEVIOLET_CHEESE = 'cheese-violet';

  Map.prototype.getAvailableCellTypes = function () {
    return [this.WALL, this.PLAYER, this.CHEESE, this.NOTHING, this.MOUSENORTH, this.MOUSEEAST, this.MOUSESOUTH, this.MOUSEWEST,
            this.MOUSEGREY, this.MOUSEBLUE, this.MOUSEBRIGHTBLUE, this.MOUSEBRIGHTGREEN, this.MOUSEDARKGREEN, this.MOUSEYELLOW,
            this.MOUSERED, this.MOUSEORANGE, this.MOUSEVIOLET,
            this.MOUSEGREY_CHEESE, this.MOUSEBLUE_CHEESE, this.MOUSEBRIGHTBLUE_CHEESE, this.MOUSEBRIGHTGREEN_CHEESE,
            this.MOUSEDARKGREEN_CHEESE, this.MOUSEYELLOW_CHEESE,
            this.MOUSERED_CHEESE, this.MOUSEORANGE_CHEESE, this.MOUSEVIOLET_CHEESE];
  };

  return Map;
});