# Fortgeschrittene objektorientierte Programmierung
Technische Universität Wien, SS2015

## Teammitglieder

 * Lisa Fichtinger
 * Patrick Säuerl
 * Thomas Rieder

## Aufgabe 1

Zum Ausführen einfach ein `npm install` im `backend`-Ordner gefolgt von `npm start`. HTTP lauscht auf Port `3000` und WebSocketes auf Port `8080`. Zum spielen einfach `http://localhost:3000` öffnen.

> Entwickeln Sie in einer objektorientierten Programmiersprache Ihrer Wahl (aber in keiner auf die Spieleprogrammierung spezialisierten Sprache) ein Mäuserennspiel entsprechend folgender Beschreibung.
> Auf dem Spielfeld befindet sich ein labyrinthartiges Geflecht an Mauern und Gängen. Etwa in der Mitte befindet sich genau ein Ausgang, an dem ein für Mäuse attraktiver und von Weitem riechbarer Futterköder liegt. An den Seiten oder Ecken befinden sich mehrere Eingänge. Die am Mäuserennen teilnehmenden hungrigen Mäuse betreten das Spielfeld gleichzeitig durch zufällig gewählte Eingänge und streben rasch und kerzengerade auf die Richtung zu, aus der sie den Geruch des Futterköders wahrnehmen. Wenn ihnen eine Mauer im Weg steht, laufen sie in jener Richtung an der Mauer entlang, die eher in Richtung des wahrgenommenen Geruchs liegt. Wenn Mäuse in eine Sackgasse geraten, laufen sie ein kurzes Stück in eine zufällig gewählte andere, nicht durch eine Mauer blockierte Richtung. Danach beginnen sie aber wieder damit, in möglichst direkter Linie in Richtung des Geruchs des Futterköders zu laufen. Wenn sich zwei Mäuse begegnen, beschnuppern sie sich zunächst, das heißt, sie legen eine Pause ein. Danach trennen sie sich und laufen ein Stück in zufällig gewählte unterschiedliche Richtungen, bevor sie wieder auf das Ziel zustreben. Gewinnerin ist jene Maus, die als erste den Ausgang erreicht.

> Der Lauf der Mäuse wird ausschließlich durch den Computer automatisch gesteuert. Spieler(innen) können das Wettrennen dadurch beeinflussen, dass sie Wind in eine der vier Himmelsrichtungen erzeugen, der die Richtung beeinflusst, aus der der Geruch des Futterköders wahrgenommen wird. Südwind verschiebt die wahrgenommene Richtung nach Norden, Nordwind nach Süden, Westwind nach Osten und Ostwind nach Westen. Jede(r) Spieler(in) kümmert sich um eine z.B. farblich gekennzeichnete Maus und versucht, diese durch Erzeugen von Wind möglichst rasch zum Ausgang zu führen. Gleichzeitig kann man dadurch andere Mäuse behindern.

> Jede(r) Spieler(in) befindet sich auf dem eigenen Computer und ist mit den Computern der anderen Spieler(innen) über ein Netzwerk verbunden. Die Kommunikation zwischen den Computern soll so effizient ablaufen, dass zumindest alle Gruppenmitglieder gleichzeitig spielen können, ohne nennenswerte Verzögerungen zu bemerken.

> Suchen Sie selbst nach geeigneten Steuerungsmöglichkeiten für das Erzeugen des Winds und weitere Details des Spielablaufs. Achten Sie darauf, dass das Spiel spannend wird. Vielleicht wäre es sinnvoll, den Wind nur langsam zu- und abnehmen zu lassen und die Windstärke davon abhängig zu machen, wie viele Spieler gerade Wind zu erzeugen versuchen. Eventuell kann der erzeugte Wind durch zufälligen natürlichen Wind überlagert werden. Der Fantasie sind hier keine Grenzen gesetzt. Damit das Spiel interssanter wird, sollte es zumindest mehrere Levels mit unterschiedlich gestalteten Spielfeldern geben.

## Aufgabe 2

> Schreiben Sie eine neue Variante des Mäuserennpiels aus der 1. Aufgabe, wobei das ganze Spiel aber nur auf einem Rechner in einem Fenster laufen soll. Jeder Maus sind eigene Tasten zur Steuerung des Windes zugeordnet, sodass mehrere Spieler gleichzeitig über dieselbe Tastatur den Wind beeinflussen können, möglichst ohne sich in die Quere zu kommen. Die Reaktionszeit nach einem Tastendruck muss kurz sein um sinnvoll spielen zu können. Achtung: Ineffiziente Programmteile führen in Smalltalk leicht zu merkbaren Verzögerungen im Programmablauf.

### Tutorials, Helpful Links

 * [Wiki Squeak](http://wiki.squeak.org/squeak)
 * [Smalltalk Overview](http://web.cecs.pdx.edu/~harry/musings/SmalltalkOverview.html)
 * [Squeak Basics](http://wiki.squeak.org/squeak/4)
 * [Smalltalk Syntax](http://rigaux.org/language-study/syntax-across-languages-per-language/Smalltalk.html)
 * [Smalltalk Language Reference](http://www.angelfire.com/tx4/cus/notes/smalltalk.html)

### Squeak und Monticello aufsetzen
Von [Squeak](http://www.squeak.org/Downloads) den zip ordner Squeak All-in-One downloaden und entpacken. ``Squeak.bat`` oder ``Squeak.sh`` ausführen. Fertig!

Wir haben 2 Packages, mit denen wir arbeiten, das ``Morphic`` Package und ``MouseRace`` Package. Dazu Monticello Browser starten - *Tools - Monticello Browser*. Button *+Repository* und den git ordner ``aufgabe2/monticello`` auswählen. Mit Button *Open* kann man die Versionen laden. Linke Spalte sind Packages, Rechte Spalte sind Versionen. Klick auf *Load*.

Nun den System-Browser starten - *Tool - Browser*. Da gibt es wo die Kategorie ``MouseRace-Base``. Für weitere Kategorien zum Package ``MouseRace``, muss der Kategorienname ``MouseRace-xxx`` sein.

Mit *Tool - Workspace* zB ``MouseRaceMorph new.`` - das Race window startet.

Zum Schluss Änderungen ins Monticello einchecken. Package in der linken Spalte auswählen (wenn * dabei hat sich was geändert) - rechte Spalte repository auswählen - Button *Save*

Wenn man Squeak schließt und er fragt ob man Änderungen speichern will, bezieht sich das auf die geöffneten Tool Fenster. Beim nächsten Start werden die dann wieder alle geöffnet.


## Aufgabe 3

### Links ###

 * [Aufgabenstellung](http://www.complang.tuwien.ac.at/franz/foop15s3)
 * [Folien zu Eiffel](http://www.complang.tuwien.ac.at/franz/foop/foop15s4.pdf)
 * [Eiffel for beginners](http://www.maths.tcd.ie/~odunlain/eiffel/eiffel_course/eforb.htm)


#### CAT-CALLS:

 * [Eiffel Doku zu dem Thema](https://docs.eiffel.com/book/method/et-inheritance#Covariance.2C_anchored_declarations.2C_and_.22catcalls.22)
 * [Beispiele](http://tecomp.sourceforge.net/index.php?file=doc/papers/lang/catcall_solution.txt)

Example 3 ist genius :D
Info: Cat erbt von Animal ... List<Cat> ist zu List<Animal> covariant ( siehe [Wikipedia](http://en.wikipedia.org/wiki/Covariance_and_contravariance_%28computer_science%29))

Beispiel was wir heir bringen könnten:

Vererbungshierarchie: 
 * Game, DevGame, NormGame
 * Person, Dev, Player
 * DevGame erbt von Game, und erlaubt nur Developers, NormGame erlaubt alle.

Wir erzeugen ein NormGame, nehmen es als Game an und geben dorthin einen Dev => Laufzeitfehler
das wäre ein Cat-Call. Muss aber nicht enhalten sein ... kann auch extra gezeigt werden.


#### IDE

ISE Eiffel, wie von ihm Vorgeschlagen. 
Ich hab keine Gegenargumente gefunden.

#### How to Open

[IDE](https://www.eiffel.com/forms/download-eiffelstudio/)

To open the project go to:

File > Open Project > Add Project

Go to your FOOP folder .. choose ``FOOP3.ecf``

Documentation:

 * https://docs.eiffel.com/book/eiffelstudio/running-and-debugging
 * http://tecomp.sourceforge.net/index.php?file=doc/lang/tutorial.txt


#### Ziel lernen ###

* dynamisch überprüften Zusicherungen,
* kovarianten Eingangsparametertypen und "CAT-Calls"
* sowie einigen weiteren praktischen Eigenschaften von Eiffel


#### AppDesign - Rennspielverwaltung ###

(runden fahren, F1 ..., nicht die Mäuse und der Käse ^^).
Consolen App ... angelehnt an schuay-foop

Es gibt:

* Personen (abstract), davon Erben Devs, Gamer und DevsGamer (erbt von Devs und von Gamer).
* Person hat: Name, toString methode abstract
* Dev überschreibt toString (DEV: <name> als ausgabe).
* Gamer überschreibt toString (Gamer <gamer> als ausgabe).
* DevGamer überschreibt toString (DevGamer: <name>).

Es gibt einen PersonenStore wo es fix eingetragene Personen gibt (2 von jeder Stückzahl)

Es gibt ein GameLog wo man games hinzufügen kann (Datenstruktur?? .. weiß ich noch nicht).

Game besteht aus:

* Name (name)
* Duration (wie lange dauerte es in Sekunden)
* Laps (wieviele laps mussten gefahren werden)
* Partcipations (teilname an einem Spiel ... ist ein Array).

Gibt eine Ableitung von Game, wo nur DevPartictpations sein dürfen.

Paritciapation besteht aus:

* Array mit lap time (für zusicherungen).
* Person die teilnimmt.

DevParticiatpion besteht aus:

* Person die teilnimmt darf nur Dev sein.

Commands:

* ShowPersons (printet den personsnstore)
* ShowGames (printet das GameLog)
* CreateGame (legt ein neues Spiel an - entscheiden ob DevGame oder normales Game))
* AddPartictipation (fügt einen Spieler hinzu zum Game - entscheiden ob DevParticiatpion oder normale Paritciapation).
* ShowGame (zeigt ein einzelnes game an).
* Exit
* NotAvailableCommand


#### Zu Beantwortende Fragen ###

1.) Wie hoch ist der Aufwand um Zusicherungen im Eiffel-Code zu formulieren?
ToDo - ergibt sich 


2.) Wie stark wirkt sich die Überprüfung von Zusicherungen auf die Laufzeit aus?
ToDo - ergibt sich 


3.) Vorbedingungen dürfen im Untertyp nicht stärker und Nachbedingungen nicht schwächer werden um Ersetzbarkeit zu garantieren. Der Eiffel-Compiler überprüft diese Bedingungen. Ist es (trotz eingeschalteter Überprüfung von Zusicherungen) möglich, diese Bedingungen zu umgehen? Wenn ja, wie?
ToDo ... kein plan grade und beim googeln nichts gefunden
Evtl: kovarianten eingangstyp machen und diesen beschränken .... 


4.) Eiffel erlaubt kovariante Eingangsparametertypen. Unter welchen Bedingungen führt das zu Problemen, und wie äußern sich diese? Schreiben Sie ein Programm, in dem die Verwendung kovarianter Eingangsparametertypen zu einer Exception führt.
Können wir aus dem Link kopieren oder wir bauen es bei unserem Programm ein.
    

5.) Vereinfachen kovariante Eingangsparametertypen die Programmierung? Unter welchen Bedingungen ist das so?
http://tecomp.sourceforge.net/index.php?file=doc/papers/lang/catcall_solution.txt
Covariant argument redefinition in subtypes (a subtype relation allows polymorphic attach) violate the Liskov Substitution Principle (LSP). Inheritance with covariant redefinition does not allow that the subtype (descendent) can be substituted in all aspects to an entity of its supertype (parent). 
Also ... erleichternd as schreiben ... bauen aber auch potenzielle Fehlermöglichkeiten ein.
Überlegung: Ja vereinfacht das ganze, aber nur wenn ich nicht nach oben caste. Dann grenze ich quasi neu ein. Bricht das ganze aber.
    

6.) Welche Spracheigenschaften von Eiffel finden Sie interessant und würden Sie gerne auch in anderen Sprachen sehen? Welche Eigenschaften von Eiffel empfinden Sie dagegen als störend?
Er will wohl Liskov hören ...